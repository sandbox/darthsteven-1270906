<?php
/**
 * @file
 *   The Language compliant path prefixes module.
 *
 *
 *   See: http://drupal.org/sandbox/darthsteven/1270906 for why we exist.
 */

/**
 * Implements hook_boot().
 *
 * This is just to make sure we're around from very, very early in the
 * bootstrap.
 */
function language_compliant_path_prefixes_boot() {

}

/**
 * Implements hook_language_negotiation_info_alter().
 */
function language_compliant_path_prefixes_language_negotiation_info_alter(array &$language_providers) {
  foreach ($language_providers as $id => $providers) {
    if (isset($provider['callback']['url_rewrite']) && ($provider['callback']['url_rewrite'] == 'locale_language_url_rewrite_url')) {
      $language_providers[$id]['callback']['url_rewrite'] = 'language_compliant_path_prefixes_locale_language_url_rewrite_url';
    }
  }
}

/**
 * Rewrite URLs for the URL language provider.
 *
 * We allow other modules to adjust the path prefix too. See the MARKED LINE.
 */
function language_compliant_path_prefixes_locale_language_url_rewrite_url(&$path, &$options) {
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['languages'] = &drupal_static(__FUNCTION__);
  }
  $languages = &$drupal_static_fast['languages'];

  if (!isset($languages)) {
    $languages = language_list('enabled');
    $languages = array_flip(array_keys($languages[1]));
  }

  // Language can be passed as an option, or we go for current URL language.
  if (!isset($options['language'])) {
    global $language_url;
    $options['language'] = $language_url;
  }
  // We allow only enabled languages here.
  elseif (!isset($languages[$options['language']->language])) {
    unset($options['language']);
    return;
  }

  if (isset($options['language'])) {
    switch (variable_get('locale_language_negotiation_url_part', LOCALE_LANGUAGE_NEGOTIATION_URL_PREFIX)) {
      case LOCALE_LANGUAGE_NEGOTIATION_URL_DOMAIN:
        if ($options['language']->domain) {
          // Ask for an absolute URL with our modified base_url.
          $options['absolute'] = TRUE;
          $options['base_url'] = $options['language']->domain;
        }
        break;

      case LOCALE_LANGUAGE_NEGOTIATION_URL_PREFIX:
        if (!empty($options['language']->prefix)) {
          // MARKED LINE: here we don't just replace the prefix, but prefix it.
          $options['prefix'] = $options['language']->prefix . '/' . $options['prefix'];
        }
        break;
    }
  }
}
